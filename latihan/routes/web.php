<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('index');
});

Route::get('/form', 'FormController@form');
Route::get('/kirim', 'FormController@kirim');
route::get('/master', function(){
    return view('layout.master');
});
route::get('/datatabel', function(){
    return view('tabel.datatabel');
});
route::get('/tabel', function(){
    return view('tabel.tabel');
});